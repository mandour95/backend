<html>

<head>
    <title>Form Inputs Types</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>
    <div class="container">
        <h1>Register here</h1>
        <form method="post" action="Index.php">
            <fieldset>
                <legend>Form</legend>
                <div class="form-group">
                    <label for="first name">First name:</label>
                    <input type="text" class="form-control" placeholder="Enter first name" name="name">
                </div>
                <div class="form-group">
                    <label for="email">Email address:</label>
                    <input type="email" class="form-control" placeholder="Enter email" name="email">
                </div>
                <div class="form-group">
                    <label for="phone">Enter a phone number:</label><br><br>
                    <input type="tel" class="form-control" id="phone" name="phone" placeholder="0123456789" pattern="[0-9]{11}" required>
                </div>
                <div class="form-group">
                    <label for="password">Password:</label>
                    <input type="password" class="form-control" placeholder="Enter password" name="password">
                </div>
                <br>
                <div>
                    <label for="birth-date">Birthday:</label>
                    <input type="date" class="form-control" id="birth-date" name="birth-date">
                </div>
                <br>
                <div class="form-group">
                    <label for="meeting-time">Choose a time for your appointment:</label>

                    <input type="datetime-local" class="form-control" name="meeting-time" value="2018-06-12T19:30">
                </div>
                <div class="form-group">
                    <label for="browser">Choose a browser from this list:</label>
                    <input list="browsers" class="form-control" id="browser" name="browser" />
                    <datalist id="browsers">
                        <option value="Chrome">
                        <option value="Firefox">
                        <option value="Internet Explorer">
                        <option value="Opera">
                        <option value="Safari">
                        <option value="Microsoft Edge">
                    </datalist>
                </div>
                <div class="form-group">
                    <label for="gender">Choose your gender:</label>
                    <br>
                    <input type="radio" id="male" name="gender" value="male">
                    <label for="male">Male</label><br>
                    <input type="radio" id="female" name="gender" value="female">
                    <label for="female">Female</label><br>
                    <input type="radio" id="other" name="gender" value="other">
                    <label for="other">Other</label>
                </div>
                <div class="form-group">
                    <label for="vechiles">Choose your vechile:</label>
                    <br>
                    <input type="checkbox" id="vehicle" name="vehicle" value="Bike">
                    <label for="vehicle"> I have a bike</label><br>
                    <input type="checkbox" id="vehicle" name="vehicle" value="Car">
                    <label for="vehicle"> I have a car</label><br>
                    <input type="checkbox" id="vehicle" name="vehicle" value="motorbike">
                    <label for="vehicle"> I have a motorbike</label>
                </div>
                <div class="form-group">
                    <label for="favColor">Select your favorite color:</label>
                    <input type="color" id="favColor" name="favColor" value="#ff0000">
                </div>
                <div class="form-group">
                    <label for="quantity">Participants (max 5):</label>
                    <input type="number" class="form-control" id="quantity" name="quantity" min="1" max="5">
                </div>

                <div class="form-group">
                    <label for="myFile">Select a file:</label>
                    <input type="file" class="form-control" id="myFile" name="myFile">
                </div>
                <div class="form-group">
                    <label for="homepage">Add your website:</label>
                    <input type="url" class="form-control" id="homepage" name="website">
                </div>
                <div class="form-group">
                    <label for="gsearch">Search Google:</label>
                    <input type="search" class="form-control" id="gsearch" name="search">
                </div>
                <input type="image" class="form-control" id="image" src="#">
                <br>
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="reset" class="btn btn-primary" value="Reset">Reset</button>
            </fieldset>
        </form>
    </div>

    <div class="container">
        <h1>Thank you</h1>
        <p>Here is the information you have submitted</p>
        <ol>
            <li><em>Name:</em> <?php echo $_POST["name"] ?></li>
            <li><em>E-mail:</em> <?php echo $_POST["email"] ?></li>
            <li><em>Phone number:</em> <?php echo $_POST["phone"] ?></li>
            <li><em>Birth date:</em> <?php echo $_POST["birth-date"] ?></li>
            <li><em>Meeting Time:</em> <?php echo $_POST["meeting-time"] ?></li>
            <li><em>Browser:</em> <?php echo $_POST["browser"] ?></li>
            <li><em>Gender:</em> <?php echo $_POST["gender"] ?></li>
            <li><em>vehicle:</em> <?php echo $_POST["vehicle"] ?></li>
            <li><em>Fav color:</em> <?php echo $_POST["favColor"] ?></li>
            <li><em>Participants:</em> <?php echo $_POST["quantity"] ?></li>
            <li><em>Your file:</em> <?php echo $_POST["myFile"] ?></li>
            <li><em>Your website:</em> <?php echo $_POST["website"] ?></li>
        </ol>
    </div>
</body>

</html>