<?php
session_start();
?>

<html>

<head>
    <title>Home Page</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>


<body>
    <div class="container">
        <form method="post" action="Home.php">
            <div class="form-group">
                <label for="my_file">Please upload your image:</label>
                <input type="file" class="form-control" id="my_file" name="my_file">
            </div>
            <button type="submit" class="btn btn-primary form-control">Log in</button>
        </form>
    </div>
</body>

</html>

<?php
$_SESSION['e_mail'] = $_POST['email'];
$allowedExts = array("gif", "jpeg", "jpg", "png");

if (isset($_POST['my_file'])) {
    $my_file = $_FILES['file']['tmp_name'];
    $phototype = $_FILES['file']['type'];

    if (($_FILES["file"]["type"]) == "image.gif" || ($_FILES["file"]["type"]) == "image.jpeg" || ($_FILES["file"]["type"]) == "image.jpg" || ($_FILES["file"]["type"]) == "image.png") {
        header("Location:Home.php");
    } else {
        echo "Please upload a valid Image";
    }
}
?>