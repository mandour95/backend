<?php
session_start();
?>

<html>

<head>
    <title>Home Page</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>

<body>
    <div class="container">
        <h1>Welcome</h1>
        <ol>
            <li><?php
                echo "E-mail:" . $_SESSION['e_mail'];
                ?></li>
            <li><img src="<?php echo $_POST["my_file"] ?>" alt="Image"></li>

        </ol>
    </div>
</body>

</html>